import math


def get_boxes_count(products_count, box_capacity):
    assert products_count != '', 'Ошибка! Количество продуктов, которое нужно отправить, ' \
                                'не должно быть пустым.'
    assert box_capacity != '', 'Ошибка! Количество продуктов, которое помещается в коробку, ' \
                              'не должно быть пустым.'
    assert type(products_count) == int, 'Ошибка! Количество продуктов, которое нужно отправить, ' \
                                                  'должно иметь целочисленный формат.'
    assert type(box_capacity) == int, 'Ошибка! Количество продуктов, которое помещается в коробку, ' \
                                              'должно иметь целочисленный формат.'
    assert products_count > 0, 'Ошибка! Количество продуктов, которое нужно отправить, ' \
                               'должно быть больше нуля.'
    assert box_capacity > 0, 'Ошибка! Количество продуктов, которое помещается в коробку, ' \
                             'должно быть больше нуля.'
    boxes_count = math.ceil(products_count / box_capacity)
    return boxes_count