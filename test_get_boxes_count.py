import pytest
from function_get_boxes_count import get_boxes_count


@pytest.mark.parametrize(['products_count_for_positive_tests', 'box_capacity_for_positive_tests'],
                         [(1200, 25), (54, 54), (123, 1), (1, 19), (99999999999999999999, 110111270141801),])
def test_get_boxes_count_positive(products_count_for_positive_tests, box_capacity_for_positive_tests):
    get_boxes_count(products_count_for_positive_tests, box_capacity_for_positive_tests)


@pytest.mark.xfail
@pytest.mark.parametrize(['products_count_for_negative_tests', 'box_capacity_for_negative_tests'],
                         [('', 125), (8, ''), (-27, 8), (2, -15), (0.15, 71), (19, 8.3), ('один', 4), (16, 'two'),
                          (0, 11), (58, 0), ('', -75), (2.22, '8'), (-0.9, 0)])
def test_get_boxes_count_negative(products_count_for_negative_tests, box_capacity_for_negative_tests):
    get_boxes_count(products_count_for_negative_tests, box_capacity_for_negative_tests)